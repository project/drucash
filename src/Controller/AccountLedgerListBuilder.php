<?php

namespace Drupal\drucash\Controller;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;

class AccountLedgerListBuilder extends EntityListBuilder{

    public function buildHeader(){
        $header = [];
        $header['id'] = $this->t('ID');
        $header['date'] = $this->t('Date');
        $header['name'] = $this->t('Name');
        $header['balance'] = $this->t('Balance');
        return $header + parent::buildHeader();
    }

    public function buildRow(EntityInterface $entity){
        /** @var Transaction $entity */

        $date = $entity->getDate();

        $row = [];
        $row['id'] = $entity->id();
        $row['date'] = $date ? $date->format('d/m/Y') : '';
        $row['name'] = $entity->label();
        $row['balance'] = '0.0';
        return $row + parent::buildRow($entity);
    }

    public function getOperations(EntityInterface $entity){
        $actions = parent::getOperations($entity);
        return $actions;
    }

    protected function getEntityListQuery(): QueryInterface{

        //Get the account ID from the url.
        $route_match = \Drupal::service('current_route_match');
        $account_id = $route_match->getParameter('drucash_account');

        $query = $this->getStorage()->getQuery()
        ->accessCheck(TRUE)
        ->sort('date');

        $group = $query->orConditionGroup();
        $group->condition('from', $account_id);
        $group->condition('to', $account_id);

        $query->condition($group);

      // Only add the pager if a limit is specified.
      if ($this->limit) {
        $query->pager($this->limit);
      }

      return $query;
    }

}