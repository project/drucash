<?php

namespace Drupal\drucash\Entity;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Account entity.
 *
 * @ingroup drucash
 *
 * @ContentEntityType(
 *   id = "drucash_account",
 *   label = @Translation("Account"),
 *   label_collection = @Translation("Accounts"),
 *   label_singular = @Translation("account"),
 *   label_plural = @Translation("accounts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count account",
 *     plural = "@count accounts",
 *   ),
 *   base_table = "drucash_account",
 *   admin_permission = "administer drucash_account",
 *   handlers = {
 *     "list_builder" = "Drupal\drucash\Controller\AccountListBuilder",
 *     "form" = {
 *       "add" = "Drupal\drucash\Form\AccountForm",
 *       "edit" = "Drupal\drucash\Form\AccountForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/content/account",
 *     "add-form" = "/account/add",
 *     "canonical" = "/account/{drucash_account}",
 *     "edit-form" = "/account/{drucash_account}/edit",
 *     "delete-form" = "/account/{drucash_account}/delete",
 *     "delete-multiple-form" = "/admin/content/account/delete-multiple",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
 class Account extends ContentEntityBase implements AccountInterface {

    public static function baseFieldDefinitions( EntityTypeInterface $entity_type ) {

        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['name'] = BaseFieldDefinition::create('string')
                            ->setLabel('Name')
                            ->setDescription('Account name')
                            ->setRequired(TRUE)
                            ->setDefaultValue('')
                            ->setDisplayOptions('form',[
                                'type' => 'string',
                                'weight' => 1.0,
                            ]);

        $fields['description'] = BaseFieldDefinition::create('string_long')
                            ->setLabel('Description')
                            ->setDescription('Account description')
                            ->setDefaultValue('')
                            ->setDisplayOptions('form', [
                                'type' => 'basic_string',
                                'weight' => 3.0,
                            ]);

        $fields['type'] = BaseFieldDefinition::create('list_string')
                            ->setLabel('Type')
                            ->setDescription('Account type')
                            ->setRequired(TRUE)
                            ->setSettings([
                                'allowed_values' => [
                                    'assets' => t('Assets'),
                                    'income' => t('Income'),
                                    'expenses' => t('Expenses')
                                ]
                            ])
                            ->setDisplayOptions('form', [
                                'type' => 'list_default',
                                'weight' => 2.0,
                            ]);

        $fields['children'] = BaseFieldDefinition::create('entity_reference')
                            ->setLabel('Children')
                            ->setDescription('Children accounts')
                            ->setRequired(FALSE)
                            ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
                            ->setDefaultValue([])
                            ->setSettings([
                                'target_type' => 'drucash_account'
                            ]);

        return $fields;
    }

    public function label(){
        return $this->getName();
    }

    public function getName(): string {
        return $this->get('name')->isEmpty() ? '' : $this->get('name')->first()->getString();
    }

    public function getDescription(): string {
        return $this->get('description')->isEmpty() ? '' : $this->get('description')->first()->getString();
    }

    public function getType() {
        return $this->get('type')->isEmpty() ? '' : $this->get('type')->first()->getString();
    }

    public function getTypeLabel(){
        $value = $this->get('type')->first();
        $allowed_values = $value->getDataDefinition()->getSetting('allowed_values');
        return $allowed_values[$value->getString()] ?? '';
    }

    public function getChildren(): array {
        return $this->get('children')->getValue();
    }

    public function setName( string $name ) {
        $this->set('name', $name);
    }

    public function setDescription( string $description ) {
        $this->set('description', $description);
    }

    public function setType( string $type ) {
        $this->set('type', $type);
    }

    public function addChildren( AccountInterface $child ) {
        $children_ids = $this->get('children')->getValue();
        $ids = array_column($children_ids,'target_id');
        if(!in_array($child->id(), $ids)){
            $this->get('children')->appendItem(['target_id' => $child->id()]);
        }
    }

    public function removeChildren( AccountInterface $child ){
        $children_ids = $this->get('children')->getValue();
        $key = array_search($child->id(), array_column($children_ids, 'target_id'));
        if($key !== false){
            $this->get('children')->removeItem($key);
        }
    }

 }