<?php declare(strict_types = 1);

namespace Drupal\Tests\drucash\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the UI pages for account administration.
 *
 * @group drucash
 */
final class AccountUi extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['drucash', 'block'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
  }

  /**
   * Test callback.
   */
  public function testAccessAccountsList(): void {
    $admin_user = $this->drupalCreateUser(['access administration pages','administer drucash_account']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/content/account');
    $this->assertSession()->elementExists('xpath', '//a[text() = "Add account"]');
  }

}
