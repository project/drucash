<?php declare(strict_types = 1);

namespace Drupal\Tests\drucash\Kernel;

use Drupal\drucash\Entity\Account;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the drucash_account entity.
 *
 * @group drucash
 */
final class AccountEntityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['drucash', 'options'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('drucash_account');
  }

  /**
   * Tests the drucash_account entity class.
   */
  public function testAccountCreate(): void {
    $account = Account::create([]);
    $this->assertInstanceOf(Account::class, $account);
  }

  public function testValidateWithoutRequiredFields(): void {
    $account = Account::create([]);
    $this->assertNotEmpty($account->validate());
  }

  public function testBasicAccountValidation() {
    $account = Account::create([
      'name' => 'Account',
      'type' => 'assets'
    ]);
    $this->assertEmpty($account->validate());
  }

  public function testGettersAndSetters(){
    $account = Account::create([]);
    $account->setName('Account');
    $account->setDescription('Description');
    $account->setType('assets');
    $this->assertEquals('Account', $account->getName());
    $this->assertEquals('Description', $account->getDescription());
    $this->assertEquals('assets', $account->getType());
  }

  public function testSetInvalidAccountType(){
    $account = Account::create([]);
    $account->setType('invalid');
    $this->assertNotEmpty($account->validate());
  }

  public function testDefaultAccountChildren(){
    $account = Account::create([]);
    $this->assertEmpty($account->getChildren());
  }

  public function testSetAccountChildren(){
    $parent = Account::create([]);
    
    $child = Account::create([]);
    $child->save();

    $parent->addChildren($child);
    $this->assertCount(1, $parent->getChildren());
  }

  public function testSetAccountChildrenTwice(){
    $parent = Account::create([]);
    
    $child = Account::create([]);
    $child->save();

    $parent->addChildren($child);
    $parent->addChildren($child);

    $this->assertCount(1, $parent->getChildren());
  }

  public function testSetAccountChildrenAndRemove(){
    $parent = Account::create([]);
    
    $child = Account::create([]);
    $child->save();

    $parent->addChildren($child);
    $this->assertCount(1, $parent->getChildren());
    $parent->removeChildren($child);
    $this->assertCount(0, $parent->getChildren());
  }

}
