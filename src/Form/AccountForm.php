<?php

namespace Drupal\drucash\Form;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\drucash\Entity\Account;

class AccountForm extends ContentEntityForm{

    public function buildForm( array $form, \Drupal\Core\Form\FormStateInterface $form_state){
        $form = parent::buildForm($form, $form_state);

        $entity = $this->getEntity();
        $parent = $this->getAccountParent($entity);

        $form['parent'] = [
            '#type' => 'entity_autocomplete',
            '#title' => $this->t('Parent account'),
            '#target_type' => 'drucash_account',
            '#default_value' => $parent,
            '#weight' => 1.5
        ];

        return $form;
    }

    public function submitForm(&$form, \Drupal\Core\Form\FormStateInterface $form_state){
        
        $entity = $this->getEntity();
        
        //Check if any account has the entity listed in its children field.
        $query = \Drupal::entityQuery('drucash_account')
            ->accessCheck(FALSE)
            ->condition( 'children', $entity->id(), 'CONTAINS' );

        //Remove any existing parents.
        $parents = $query->execute();
        foreach($parents as $parent_id){
            $parent = Account::load($parent_id);
            $parent->removeChildren($entity);
            $parent->save();
        }

        $parent_id = $form_state->getValue('parent');
        if($parent_id){
            $parent = Account::load($parent_id);
            $parent->addChildren($entity);
            $parent->save();
        }

        parent::submitForm($form, $form_state);
        
        $form_state->setRedirect('entity.drucash_account.collection');
    }

    protected function getAccountParent( EntityInterface $entity ): ?Account{
        //Check if any account has the entity listed in its children field.
        $query = \Drupal::entityQuery('drucash_account')
            ->accessCheck(FALSE)
            ->condition( 'children', $entity->id(), 'CONTAINS' );

        $result = $query->execute();
        if(empty($result)){
            return NULL;
        }
        $item_id = array_shift($result);
        return Account::load($item_id);
    }

}