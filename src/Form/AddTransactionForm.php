<?php

namespace Drupal\drucash\Form;
use Drupal\Core\Entity\ContentEntityForm;

class AddTransactionForm extends ContentEntityForm{

    public function buildForm( $form, \Drupal\Core\Form\FormStateInterface $form_state){
        $form = parent::buildForm($form, $form_state);
        return $form;
    }

    public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state){
        parent::submitForm($form, $form_state);

        //Get the account ID from the url.
        $route_match = \Drupal::service('current_route_match');
        $account_id = $route_match->getParameter('drucash_account');

        $form_state->setRedirect('drucash.account_ledger', ['drucash_account' => $account_id]);
    }
}