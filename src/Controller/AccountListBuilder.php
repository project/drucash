<?php

namespace Drupal\drucash\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\drucash\Entity\AccountInterface;
use Drupal\Core\Entity\EntityListBuilder;

class AccountListBuilder extends EntityListBuilder{

    public function buildHeader(){
        $header = [];
        $header['id'] = $this->t('Id');
        $header['name'] = $this->t('Name');
        $header['type'] = $this->t('Type');
        $header['description'] = $this->t('Description');
        $header['balance'] = $this->t('Balance');
        return $header + parent::buildHeader();
    }

    public function buildRow( EntityInterface $entity) {
        /** @var AccountInterface $entity */
        $row = [];
        $row['id'] = $entity->id();
        $row['name'] = $entity->getName();
        $row['type'] = $entity->getTypeLabel();
        $row['description'] = $entity->getDescription();
        $row['balance'] = '0.00';
        return $row + parent::buildRow($entity);
    }

    public function getOperations(EntityInterface $entity){

        $operations = [];
        $operations['ledger'] = [
            'title' => $this->t('Ledger'),
            'weight' => 1.0,
            'url' => Url::fromRoute('drucash.account_ledger', [ 'drucash_account' => $entity->id() ]),
        ];

        return $operations + parent::getOperations($entity);
    }

}