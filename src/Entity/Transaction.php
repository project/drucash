<?php

namespace Drupal\drucash\Entity;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Defines the Transaction entity.
 *
 * @ingroup drucash
 *
 * @ContentEntityType(
 *   id = "drucash_transaction",
 *   label = @Translation("Transaction"),
 *   label_collection = @Translation("Transactions"),
 *   label_singular = @Translation("transaction"),
 *   label_plural = @Translation("transactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count transaction",
 *     plural = "@count transactions",
 *   ),
 *   base_table = "drucash_transaction",
 *   admin_permission = "administer drucash_transaction",
 *   handlers = {
 *     "list_builder" = "Drupal\drucash\Controller\AccountLedgerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\drucash\Form\AddTransactionForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *   },
 *   links = {
 *     "add-form" = "/admin/content/account/{drucash_account}/add",
 *     "canonical" = "/transaction/{drucash_transaction}",
 *     "edit-form" = "/transaction/{drucash_transaction}/edit",
 *     "delete-form" = "/transaction/{drucash_transaction}/delete",
 *     "delete-multiple-form" = "/admin/content/transaction/delete-multiple",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
 class Transaction extends ContentEntityBase implements ContentEntityInterface {

    public static function baseFieldDefinitions( EntityTypeInterface $entity_type ) {

        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['label'] = BaseFieldDefinition::create('string')
                            ->setLabel('Label')
                            ->setDescription('Transaction label')
                            ->setRequired(TRUE)
                            ->setDefaultValue('')
                            ->setDisplayOptions('form',[
                                'type' => 'string',
                            ]);

        $fields['date'] = BaseFieldDefinition::create('datetime')
                            ->setLabel('Date')
                            ->setDescription('Transaction date')
                            ->setRequired(TRUE)
                            ->setSettings([
                                'datetime_type' => 'date'
                            ])
                            ->setDisplayOptions('form', [
                                'type' => 'datetime_default'
                            ]);

        $fields['from'] = BaseFieldDefinition::create('entity_reference')
                            ->setLabel('Origin')
                            ->setDescription('Origin account')
                            ->setRequired(TRUE)
                            ->setDisplayOptions( 'form', [
                                'type' => 'entity_reference_autocomplete'
                            ])
                            ->setSettings([
                                'target_type' => 'drucash_account'
                            ]);

        $fields['to'] = BaseFieldDefinition::create('entity_reference')
                            ->setLabel('Destination')
                            ->setDescription('Destination account')
                            ->setRequired(TRUE)
                            ->setDisplayOptions( 'form', [
                                'type' => 'entity_reference_autocomplete'
                            ])
                            ->setSettings([
                                'target_type' => 'drucash_account'
                            ]);

        $fields['amount'] = BaseFieldDefinition::create('decimal')
                            ->setLabel('Amount')
                            ->setDescription('Transaction amount')
                            ->setDisplayOptions('form', [
                                'type' => 'number_decimal'
                            ]);

        return $fields;
    }

    public function label(){
        return $this->get('label')->isEmpty() ? '' : $this->get('label')->first()->getString();
     }


    public function getDate(): ?\DateTime{
        if($this->get('date')->isEmpty()){
            return NULL;
        }
        
        $date_value = $this->get('date')->first()->getValue();
        return \DateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT , $date_value['value']);
    }

 }