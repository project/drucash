<?php

namespace Drupal\drucash\Entity;
use Drupal\Core\Entity\ContentEntityInterface;

interface AccountInterface extends ContentEntityInterface{

    public function getName();
    public function getDescription();
    public function getType();
    public function getTypeLabel();
    public function getChildren();
    public function setName(string $name);
    public function setDescription(string $description);
    public function setType(string $type);
    public function addChildren( AccountInterface $child);
    public function removeChildren( AccountInterface $child);

}