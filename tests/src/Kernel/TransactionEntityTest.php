<?php declare(strict_types = 1);

namespace Drupal\Tests\drucash\Kernel;

use Drupal\drucash\Entity\Transaction;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the drucash_transaction entity.
 *
 * @group drucash
 */
final class TransactionEntityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['drucash'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('drucash_transaction');
  }

  /**
   * Test callback.
   */
  public function testTransactioncreate() {
    $transaction = Transaction::create([]);
    $this->assertInstanceOf(Transaction::class, $transaction);
  }

}
